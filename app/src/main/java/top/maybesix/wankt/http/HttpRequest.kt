package top.maybesix.wankt.http

import top.maybesix.xhhttp.XHHttp
import top.maybesix.xhhttp.annotation.GET
import top.maybesix.xhhttp.callback.ObserverCallBack

/**
 * @author MaybeSix
 * @date 2020/4/16
 * @desc TODO.
 */
interface HttpRequest {
    companion object {
        val instance = XHHttp.getInstance(HttpRequest::class.java, null)
    }

    /**
     * 获取首页banner
     * @param callback ObserverCallBack
     */
    @GET("/banner/json")
    fun getBanner(callback: ObserverCallBack)

    /**
     * 获取首页banner
     * @param callback ObserverCallBack
     */
    @GET("/banner/json")
    fun getBannerApi(callback: ObserverCallBack)

}