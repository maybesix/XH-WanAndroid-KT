package top.maybesix.wankt.common

import android.app.Application
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import androidx.multidex.MultiDex
import com.hjq.bar.TitleBar
import com.hjq.bar.style.TitleBarLightStyle
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.footer.ClassicsFooter
import com.scwang.smartrefresh.layout.header.ClassicsHeader
import com.tencent.bugly.crashreport.CrashReport
import top.maybesix.wankt.R
import top.maybesix.wankt.helper.ActivityStackManager
import top.maybesix.wankt.other.AppConfig
import top.maybesix.xhhttp.XHHttp


/**
 * @author MaybeSix
 * @date 2020/3/18
 * @desc TODO.
 */
class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        initSDK(this)
    }

    /**
     * 初始化一些第三方框架
     */
    private fun initSDK(application: Application) {

        // 标题栏全局样式

        // 标题栏全局样式
        TitleBar.initStyle(object : TitleBarLightStyle(application) {
            override fun getBackground(): Drawable {
                return ColorDrawable(getColor(R.color.colorPrimary))
            }

            override fun getBackIcon(): Drawable {
                return getDrawable(R.drawable.ic_back_black)
            }
        })

        // Bugly 异常捕捉

        // Bugly 异常捕捉
        CrashReport.initCrashReport(application, AppConfig.getBuglyId(), false)

        // Crash 捕捉界面

        // Crash 捕捉界面
//        CaocConfig.Builder.create()
//            .backgroundMode(CaocConfig.BACKGROUND_MODE_SHOW_CUSTOM)
//            .enabled(true)
//            .trackActivities(true)
//            .minTimeBetweenCrashesMs(2000) // 重启的 Activity
//            .restartActivity(HomeActivity::class.java) // 错误的 Activity
//            .errorActivity(CrashActivity::class.java) // 设置监听器
//            //.eventListener(new YourCustomEventListener())
//            .apply()

        // 设置全局的 Header 构建器

        // 设置全局的 Header 构建器
        SmartRefreshLayout.setDefaultRefreshHeaderCreator { context: Context?, layout: RefreshLayout? ->
            ClassicsHeader(
                context
            ).setEnableLastTime(false)
        }
        // 设置全局的 Footer 构建器
        // 设置全局的 Footer 构建器
        SmartRefreshLayout.setDefaultRefreshFooterCreator { context: Context?, layout: RefreshLayout? ->
            ClassicsFooter(
                context
            ).setDrawableSize(20f)
        }

        // Activity 栈管理初始化

        // Activity 栈管理初始化
        ActivityStackManager.getInstance().init(application)

        XHHttp.apply {
            baseUrl = "https://www.wanandroid.com"
            isDebug = true
        }

    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        // 使用 Dex分包
        MultiDex.install(this);
    }
}