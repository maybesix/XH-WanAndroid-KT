package top.maybesix.wankt.common

import android.view.View
import androidx.annotation.StringRes
import com.gyf.immersionbar.ImmersionBar
import com.hjq.bar.TitleBar
import top.maybesix.base.base.BaseActivity
import top.maybesix.tools.toast.XHToast
import top.maybesix.wankt.R
import top.maybesix.wankt.action.TitleBarAction

/**
 * @author MaybeSix
 * @date 2020/3/18
 * @desc TODO.
 */
abstract class MyActivity : BaseActivity(), TitleBarAction {


    /**
     * 标题栏对象
     */
    private var mTitleBar: TitleBar? = null

    /**
     * 状态栏沉浸
     */
    private lateinit var mImmersionBar: ImmersionBar


    override fun initLayout() {
        super.initLayout()
        titleBar?.setOnTitleBarListener(this)
        initImmersion()
    }

    /**
     * 初始化沉浸式
     */
    protected open fun initImmersion() {
        // 初始化沉浸式状态栏
        if (isStatusBarEnabled()) {
            createStatusBarConfig().init()
            // 设置标题栏沉浸
            mTitleBar?.let {
                ImmersionBar.setTitleBar(this, mTitleBar)
            }
        }
    }

    /**
     * 是否使用沉浸式状态栏
     */
    protected open fun isStatusBarEnabled(): Boolean {
        return true
    }

    /**
     * 状态栏字体深色模式
     */
    protected open fun isStatusBarDarkFont(): Boolean {
        return true
    }

    /**
     * 初始化沉浸式状态栏
     */
    open fun createStatusBarConfig(): ImmersionBar {
        // 在BaseActivity里初始化
        mImmersionBar = ImmersionBar.with(this) // 默认状态栏字体颜色为黑色
            .statusBarDarkFont(isStatusBarDarkFont())
        return mImmersionBar
    }

    /**
     * 获取状态栏沉浸的配置对象
     */
    open fun getStatusBarConfig(): ImmersionBar {
        return mImmersionBar
    }

    /**
     * 设置标题栏的标题
     */
    override fun setTitle(@StringRes id: Int) {
        title = getString(id)
    }

    /**
     * 设置标题栏的标题
     */
    override fun setTitle(title: CharSequence?) {
        super<BaseActivity>.setTitle(title)
        mTitleBar?.title = title
    }

    /**
     * 获取标题栏
     */
    override fun getTitleBar(): TitleBar? {
        if (mTitleBar == null) {
            mTitleBar = findTitleBar(getContentView())
        }
        return mTitleBar
    }

    /**
     * 标题栏的返回按钮
     */
    override fun onLeftClick(v: View?) {
        onBackPressed()
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.activity_left_in, R.anim.activity_left_out)
    }

    fun toast(s:String) {
        XHToast.showToast(s)
    }

}