package top.maybesix.wankt.ui.activity

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.view.View
import com.gyf.immersionbar.BarHide
import com.gyf.immersionbar.ImmersionBar
import kotlinx.android.synthetic.main.activity_splash.*
import top.maybesix.wankt.R
import top.maybesix.wankt.common.MyActivity
import top.maybesix.wankt.feature.home.HomeActivity
import top.maybesix.wankt.other.AppConfig

/**
 * @author MaybeSix
 * @date 2020/3/18
 * @desc TODO.
 */
class SplashActivity : MyActivity() {

    override fun getLayoutId() = R.layout.activity_splash


    override fun initView() {

        // 设置动画监听
        iav_splash.addAnimatorListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                HomeActivity.start()
                finish()
            }
        })
        //设置是否出现debug图标
        tv_splash_debug.visibility = if (AppConfig.isDebug()) {
            View.VISIBLE
        } else {
            View.INVISIBLE
        }
    }

    override fun initData() {

    }

    override fun createStatusBarConfig(): ImmersionBar {
        return super.createStatusBarConfig() // 有导航栏的情况下，activity全屏显示，也就是activity最下面被导航栏覆盖，不写默认非全屏
            .fullScreen(true) // 隐藏状态栏
            .hideBar(BarHide.FLAG_HIDE_STATUS_BAR) // 透明导航栏，不写默认黑色(设置此方法，fullScreen()方法自动为true)
            .transparentNavigationBar()
    }

    override fun onBackPressed() {
        //禁用返回键
        //super.onBackPressed();
    }


}