package top.maybesix.wankt.ui.copy

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import top.maybesix.wankt.R

/**
 * @author MaybeSix
 * @date 2020/3/18
 * @desc 用于拷贝的快速万能适配器.
 */
class CopyAdapter(dataList: MutableList<String>) : BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_copy, dataList) {

    /**
     * Implement this method and use the helper to adapt the view to the given item.
     *
     * 实现此方法，并使用 helper 完成 item 视图的操作
     *
     * @param helper A fully initialized helper.
     * @param item   The item that needs to be displayed.
     */
    override fun convert(helper: BaseViewHolder, item: String) {
        TODO("Not yet implemented")
    }

}