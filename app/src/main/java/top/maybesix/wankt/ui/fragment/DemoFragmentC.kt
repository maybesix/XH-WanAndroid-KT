package top.maybesix.wankt.ui.fragment

import top.maybesix.wankt.R
import top.maybesix.wankt.common.MyFragment
import top.maybesix.wankt.feature.home.HomeActivity

/**
 * @author MaybeSix
 * @date 2020/4/8
 * @desc TODO.
 */
class DemoFragmentC : MyFragment<HomeActivity>() {

    companion object {

        fun newInstance(): DemoFragmentC {
            return DemoFragmentC()
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_demo_c
    }

    override fun initView() {

    }

    override fun initData() {

    }


}