package top.maybesix.wankt.ui.fragment

import top.maybesix.wankt.R
import top.maybesix.wankt.common.MyFragment
import top.maybesix.wankt.feature.home.HomeActivity

/**
 * @author MaybeSix
 * @date 2020/4/8
 * @desc TODO.
 */
class DemoFragmentD : MyFragment<HomeActivity>() {

    companion object {

        fun newInstance(): DemoFragmentD {
            return DemoFragmentD()
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_demo_d
    }

    override fun initView() {

    }

    override fun initData() {

    }


}