package top.maybesix.wankt.ui.copy

import top.maybesix.wankt.R
import top.maybesix.wankt.common.MyFragment

/**
 * @author MaybeSix
 * @date 2020/3/18
 * @desc TODO.
 */
class CopyFragment : MyFragment<CopyActivity>() {

    companion object {

        fun newInstance(): CopyFragment {
            return CopyFragment()
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_copy
    }

    override fun initView() {

    }

    override fun initData() {

    }


}