package top.maybesix.wankt.other

import top.maybesix.wankt.BuildConfig

/**
 * @author MaybeSix
 * @date 2020/3/18
 * @desc TODO.
 */
object AppConfig {
    /**
     * 当前是否为 Debug 模式
     */
    @JvmStatic
    fun isDebug(): Boolean {
        return BuildConfig.DEBUG
    }

    /**
     * 获取当前应用的包名
     */
    @JvmStatic
    fun getPackageName(): String {
        return BuildConfig.APPLICATION_ID
    }

    /**
     * 获取当前应用的版本名
     */
    @JvmStatic
    fun getVersionName(): String {
        return BuildConfig.VERSION_NAME
    }

    /**
     * 获取当前应用的版本码
     */
    @JvmStatic
    fun getVersionCode(): Int {
        return BuildConfig.VERSION_CODE
    }

    /**
     * 获取当前应用的渠道名
     */
    @JvmStatic
    fun getProductFlavors(): String {
        return BuildConfig.FLAVOR
    }

    /**
     * 获取 BuglyId
     */
    @JvmStatic
    fun getBuglyId(): String {
        return BuildConfig.BUGLY_ID
    }
}