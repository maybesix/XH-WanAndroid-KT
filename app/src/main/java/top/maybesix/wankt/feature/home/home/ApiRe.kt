package top.maybesix.wankt.feature.home.home

import top.maybesix.xhhttp.annotation.Rename
import top.maybesix.xhhttp.model.ApiResult

/**
 * @author MaybeSix
 * @date 2020/4/22
 * @desc TODO.
 */
class ApiRe() : ApiResult<List<String>>() {

    @Rename("errorCode")
    override var code: Int = 0

    @Rename("errorMsg")
    override var msg: String = ""
}