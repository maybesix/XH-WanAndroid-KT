package top.maybesix.wankt.feature.home.home

import android.widget.ImageView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_demo_a.*
import top.maybesix.wankt.R
import top.maybesix.wankt.common.MyFragment
import top.maybesix.wankt.feature.home.HomeActivity
import top.maybesix.wankt.http.HttpRequest
import top.maybesix.xhhttp.dsl.callbackApiOf
import java.util.*


/**
 * @author MaybeSix
 * @date 2020/4/8
 * @desc TODO.
 */
class HomeFragment : MyFragment<HomeActivity>() {

    companion object {

        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_demo_a
    }

    override fun initView() {
        banner_guide_content.setAdapter { banner, itemView, model, position ->
            Glide.with(this)
                .load(model)
                .centerCrop()
                .dontAnimate()
                .into(itemView as ImageView)
        }
        banner_guide_content.setData(
            Arrays.asList("网络图片路径1", "网络图片路径2", "网络图片路径3"),
            Arrays.asList("提示文字1", "提示文字2", "提示文字3")
        )

        button.setOnClickListener {

            HttpRequest.instance.getBanner(callbackApiOf<ApiRe, List<String>> {
                success {
                    log("success code:${it.code}")
                    log(" success obj:$it")
                }
                failed {
                    log("failed code:${it.code}")
                    log("failed obj:$it")
                }
            })






//            val isChild =(ApiResult::class.java).isAssignableFrom(ApiTest::class.java)
//            log("是否是子类：" + isChild)
        }
    }

    override fun initData() {

    }


}
