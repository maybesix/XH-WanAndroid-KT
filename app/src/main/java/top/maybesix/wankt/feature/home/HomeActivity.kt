package top.maybesix.wankt.feature.home

import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import com.blankj.utilcode.util.ActivityUtils
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_home.*
import top.maybesix.tools.ext.Toasts.toast
import top.maybesix.wankt.R
import top.maybesix.wankt.common.BaseFragmentAdapter
import top.maybesix.wankt.common.MyActivity
import top.maybesix.wankt.common.MyFragment
import top.maybesix.wankt.feature.home.home.HomeFragment
import top.maybesix.wankt.helper.ActivityStackManager
import top.maybesix.wankt.helper.DoubleClickHelper
import top.maybesix.wankt.other.KeyboardWatcher
import top.maybesix.wankt.ui.fragment.DemoFragmentB
import top.maybesix.wankt.ui.fragment.DemoFragmentC
import top.maybesix.wankt.ui.fragment.DemoFragmentD

/**
 * @author MaybeSix
 * @date 2020/4/8
 * @desc TODO.
 */
class HomeActivity : MyActivity(),
    KeyboardWatcher.SoftKeyboardStateListener,
    BottomNavigationView.OnNavigationItemSelectedListener {

    companion object{
        fun start(){
            ActivityUtils.startActivity(HomeActivity::class.java)
        }
    }
    private lateinit var mPagerAdapter: BaseFragmentAdapter<MyFragment<HomeActivity>>

    override fun getLayoutId() = R.layout.activity_home


    override fun initView() {

        // 不使用图标默认变色
        bv_home_navigation.itemIconTintList = null
        bv_home_navigation.setOnNavigationItemSelectedListener(this)

        KeyboardWatcher.with(this)
            .setListener(this)
    }

    override fun initData() {
        mPagerAdapter = BaseFragmentAdapter(this)
        mPagerAdapter.addFragment(HomeFragment.newInstance())
        mPagerAdapter.addFragment(DemoFragmentB.newInstance())
        mPagerAdapter.addFragment(DemoFragmentC.newInstance())
        mPagerAdapter.addFragment(DemoFragmentD.newInstance())

        vp_home_pager.adapter = mPagerAdapter

        // 限制页面数量
        vp_home_pager.offscreenPageLimit = mPagerAdapter.count
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_home -> {
                mPagerAdapter.setCurrentItem(HomeFragment::class.java)
                return true
            }
            R.id.home_found -> {
                mPagerAdapter.setCurrentItem(DemoFragmentB::class.java)
                return true
            }
            R.id.home_message -> {
                mPagerAdapter.setCurrentItem(DemoFragmentC::class.java)
                return true
            }
            R.id.home_me -> {
                mPagerAdapter.setCurrentItem(DemoFragmentD::class.java)
                return true
            }
        }
        return false
    }
    /**
     * 软键盘收起了
     */
    override fun onSoftKeyboardClosed() {
        bv_home_navigation.visibility = View.VISIBLE
    }

    /**
     * 软键盘弹出了
     * @param keyboardHeight            软键盘高度
     */
    override fun onSoftKeyboardOpened(keyboardHeight: Int) {
        bv_home_navigation.visibility = View.GONE
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        // 回调当前 Fragment 的 onKeyDown 方法
        return if (mPagerAdapter.currentFragment.onKeyDown(keyCode, event)) {
            true
        } else super.onKeyDown(keyCode, event)
    }

    override fun onBackPressed() {
        if (DoubleClickHelper.isOnDoubleClick()) {
            // 移动到上一个任务栈，避免侧滑引起的不良反应
            moveTaskToBack(false)
            postDelayed({

                // 进行内存优化，销毁掉所有的界面
                ActivityStackManager.getInstance().finishAllActivities()
            }, 300)
        } else {
            toast(R.string.home_exit_hint)
        }
    }

    override fun onDestroy() {
        vp_home_pager.adapter = null
        bv_home_navigation.setOnNavigationItemSelectedListener(null)
        super.onDestroy()
    }


}