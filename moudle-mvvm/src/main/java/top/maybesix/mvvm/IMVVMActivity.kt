package top.maybesix.mvvm

import top.maybesix.base.base.IActivity

/**
 * @author MaybeSix
 * @date 2020/3/18
 * @desc activity的接口.
 */

interface IMVVMActivity : IActivity, IMVVMView {


}