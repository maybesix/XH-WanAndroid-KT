package top.maybesix.mvvm

/**
 * @author MaybeSix
 * @date 2020/4/29
 * @desc TODO.
 */
interface IMVVMView {


    fun showLoading() {}
    fun showSuccess() {
        showComplete()
    }

    fun showError(msg: String) {
        showComplete()
    }

    fun showComplete() {
        fun hideLoading() {}
    }

    fun showEmpty() {}

}