package top.maybesix.tools

import android.content.Context
import top.maybesix.tools.delegate.Preference
import top.maybesix.tools.toast.XHToast

/**
 *  @author: HyJame
 *  @date:   2019-09-25
 *  @desc:   KtWing 初始化
 */
object XHTools {

    fun init(context: Context) {
        Preference.init(context)
        XHToast.init(context)
    }
}