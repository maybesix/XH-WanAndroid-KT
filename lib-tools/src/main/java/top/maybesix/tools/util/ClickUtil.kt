package top.maybesix.tools.util

import top.maybesix.tools.toast.XHToast

/**
 * author：  HyZhan
 * create：  2019/8/13
 * desc：    TODO
 */
object ClickUtil {

    private var clickTime: Long = 0

    private var frequency = 1

    /**
     *  双击事件
     *  @param duration 两次间隔时间
     */
    @JvmStatic
    fun interval(count: Int = 2, duration: Int = 1000, block: () -> Unit) {
        val nowTime = System.currentTimeMillis()

        if (nowTime - clickTime > duration) {
            clickTime = nowTime
            frequency = 1
            return
        }

        frequency++

        if (frequency != count) return

        block()
        frequency = 1
    }

    private var mExitTime: Long = 0

    @JvmStatic
    fun doubleClickExit(duration: Int = 1000): Boolean {
        if (System.currentTimeMillis() - mExitTime > duration) {
            XHToast.normal("再按一次退出")
            mExitTime = System.currentTimeMillis()
            return false
        }
        return true
    }
}