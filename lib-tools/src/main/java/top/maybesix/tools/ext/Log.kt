package top.maybesix.tools.ext

import android.util.Log
import top.maybesix.tools.const.Const

/**
 *  @author: HyJame
 *  @date:   2019-09-20
 *  @desc:   Log 基本封装
 */
fun logv(message: String, tag: String = Const.TAG) = Log.v(tag, message)

fun logd(message: String, tag: String = Const.TAG) = Log.d(tag, message)

fun logi(message: String, tag: String = Const.TAG) = Log.i(tag, message)

fun logw(message: String, tag: String = Const.TAG) = Log.w(tag, message)

fun loge(message: String, tag: String = Const.TAG) = Log.e(tag, message)

fun String.showLog(tag: String = "") {
    var mTag = tag
    if (mTag == "") {
        mTag = Const.TAG
    }
    Log.d(mTag, "<<<<<<--------------------------")
    Log.d(mTag, "[content]:  $this")
    Log.d(mTag, "-------------------------->>>>>>")
}