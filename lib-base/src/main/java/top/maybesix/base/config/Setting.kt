package top.maybesix.base.config

/**
 * @author MaybeSix
 * @date 2020/3/18
 * @desc 配置
 */
object Setting {

    const val FRAGMENT_CACHE_SIZE = 100

    const val ACTIVITY_CACHE_SIZE = 100
}