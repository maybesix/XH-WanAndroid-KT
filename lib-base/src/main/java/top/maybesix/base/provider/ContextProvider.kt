package top.maybesix.base.provider

import android.app.Application
import android.content.Context


/**
 * @author MaybeSix
 * @date 2020/3/18
 * @desc 在ContextProvider中进行init.
 */
object ContextProvider {

    lateinit var application: Application

    fun attachContext(context: Context?) {
        application = context as? Application ?: throw RuntimeException("init KtArmor error !")

        //注册生命周期回调，在回调里面使用代理实现IActivity等接口
//        ActivityLifecycle.init(application)

    }
}