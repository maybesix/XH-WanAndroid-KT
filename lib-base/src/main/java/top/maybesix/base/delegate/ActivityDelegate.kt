package top.maybesix.base.delegate

import android.app.Activity
import android.os.Bundle

/**
 * @author MaybeSix
 * @date 2020/4/29
 * @desc Activity的代理接口.
 */
interface ActivityDelegate {
    fun onCreate(savedInstanceState: Bundle?)

    fun onStart()

    fun onResume()

    fun onPause()

    fun onStop()

    fun onSaveInstanceState(activity: Activity?, outState: Bundle?)

    fun onDestroy()
}