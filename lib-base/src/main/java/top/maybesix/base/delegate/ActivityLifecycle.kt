package top.maybesix.base.delegate

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.collection.LruCache
import androidx.fragment.app.FragmentActivity
import top.maybesix.base.base.IActivity
import top.maybesix.base.config.Setting
import top.maybesix.base.helper.ActivityManager

/**
 * @author MaybeSix
 * @date 2020/4/29
 * @desc Activity生命周期.
 */

object ActivityLifecycle : Application.ActivityLifecycleCallbacks {

    private val cache
            by lazy { LruCache<String, ActivityDelegate>(Setting.ACTIVITY_CACHE_SIZE) }

    fun init(application: Application) {
        application.registerActivityLifecycleCallbacks(this)
    }

    override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
        activity?.let { ActivityManager.add(it) }

        if (activity !is IActivity) {
            return
        }

        val activityDelegate = fetchActivityDelegate(activity)
            ?: newDelegate(activity)
                .apply {
                    cache.put(
                        getKey(activity), this
                    )
                }

        activityDelegate.onCreate(savedInstanceState)

        registerFragmentCallback(
            activity
        )
    }


    private fun registerFragmentCallback(activity: Activity?) {

        if (activity !is FragmentActivity) return

        activity.supportFragmentManager.registerFragmentLifecycleCallbacks(FragmentLifecycle, true)
    }

    override fun onActivityStarted(activity: Activity?) {
        fetchActivityDelegate(
            activity
        )?.onStart()
    }

    override fun onActivityResumed(activity: Activity?) {
        fetchActivityDelegate(
            activity
        )?.onResume()
    }

    override fun onActivityPaused(activity: Activity?) {
        fetchActivityDelegate(
            activity
        )?.onPause()
    }

    override fun onActivityStopped(activity: Activity?) {
        fetchActivityDelegate(
            activity
        )?.onStop()
    }

    override fun onActivityDestroyed(activity: Activity?) {
        activity?.let { ActivityManager.remove(it) }

        fetchActivityDelegate(
            activity
        )?.onDestroy()
    }

    override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
        fetchActivityDelegate(
            activity
        )?.onSaveInstanceState(activity, outState)
    }

    private fun fetchActivityDelegate(activity: Activity?): ActivityDelegate? {

        if (activity !is IActivity) {
            return null
        }

        return cache.get(
            getKey(activity)
        )
    }

    private fun newDelegate(activity: Activity): ActivityDelegate {
//        if (activity is IMvmActivity) {
//            return MvmActivityDelegateImpl(activity)
//        }

        return ActivityDelegateImpl(activity)
    }

    private fun getKey(activity: Activity): String = activity.javaClass.name + activity.hashCode()
}
