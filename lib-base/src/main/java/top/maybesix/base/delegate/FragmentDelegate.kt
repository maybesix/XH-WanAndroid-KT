package com.zhan.mvvm.delegate

import android.content.Context
import android.os.Bundle
import android.view.View

/**
 * @author MaybeSix
 * @date 2020/4/29
 * @desc Fragment代理接口.
 */

interface FragmentDelegate {

    fun onAttached(context: Context)

    fun onCreated(savedInstanceState: Bundle?)

    fun onViewCreated(v: View, savedInstanceState: Bundle?)

    fun onActivityCreate(savedInstanceState: Bundle?)

    fun onStarted()

    fun onResumed()

    fun onPaused()

    fun onStopped()

    fun onSaveInstanceState(outState: Bundle)

    fun onViewDestroyed()

    fun onDestroyed()

    fun onDetached()

    fun isAdd(): Boolean
}